﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Shield2D : MonoBehaviour
{
    public float shield = 0;
    public float speed;
    public float curX = 0;
    public bool canBreak = true;

    public void Update()
    {
        curX += speed*Time.deltaTime;
        GetComponent<MeshRenderer>().materials[0].SetTextureOffset("_MainTex", new Vector2(curX, 0));

        if(shield <= 0 && canBreak)
            Destroy(gameObject);
    }
}
