﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Missile2D : Projectile2D
{
    public ParticleSystem explosionPrefab;
    public float explosionRadius = 10;
    public float acceleration;
    public float maxSpeed;

    public void FixedUpdate()
    {
        myRigidbody.AddForce(transform.right*Mathf.Abs(acceleration));

        if(myRigidbody.velocity.magnitude > maxSpeed)
            myRigidbody.velocity = myRigidbody.velocity.normalized*maxSpeed;
    }

    public override void postContact()
    {
        Instantiate(explosionPrefab, transform.position, transform.rotation);
        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, explosionRadius, Vector2.one);

        foreach(RaycastHit2D hit in hits)
        {
            if(hit.collider.tag != targetTag)
                continue;

            Ship2D ship = hit.collider.GetComponent<Ship2D>();

            if(ship)
            {
                ship.damage(damage);

                continue;
            }

            Base enemyBase = hit.collider.GetComponent<Base>();

            if(enemyBase)
            {
                enemyBase.damage(damage);

                continue;
            }

            Shield2D shield = hit.collider.GetComponent<Shield2D>();

            if(shield)
            {
                shield.shield -= damage;
            }
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, explosionRadius);
    }
}
