﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Bullet2D : Projectile2D
{
    public float speed;

    public void FixedUpdate()
    {
        myRigidbody.MovePosition(transform.position + transform.right*speed*Time.fixedDeltaTime);
    }
}
