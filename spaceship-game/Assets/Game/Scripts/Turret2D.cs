﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Turret2D : MonoBehaviour
{
    public Bullet2D bulletPrefab;
    public GameObject spawnLocation;
    private PlayerShip2D playerTarget;
    public float attackRange = 15;
    public float turnSpeed = 5;

    public int ammo = 15;
    public int damage = 20;
    public float fireRate = 0.2f;
    private float fireDelay = 0;
    public float reloadTime = 5;
    public float reloadDelay = 0;
    public int reloadAmount = 15;
    public bool reloading = false;

    public void Start()
    {
        playerTarget = PlayerShip2D.player;
    }

    public void Update()
    {
        if(!playerTarget)
            return;

        if(Vector2.Distance(playerTarget.transform.position, transform.position) <= attackRange)
        {
            Vector3 direction = playerTarget.transform.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x)*Mathf.Rad2Deg;
            Quaternion rot = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * turnSpeed);
        }

        if(reloading)
        {
            reloadDelay -= Time.deltaTime;

            if(reloadDelay <= 0)
            {
                reloading = false;
                ammo = reloadAmount;
            }
        }
        else if(reloadDelay <= 0 && !reloading)
            {
                reloading = true;
                reloadDelay = reloadTime;
            }

        fireDelay -= Time.deltaTime;

        if(ammo > 0 && fireDelay <= 0 && Vector2.Angle(transform.right, playerTarget.transform.position - transform.position) <= 15)
        {
            Bullet2D bullet = (Bullet2D)Instantiate(bulletPrefab, spawnLocation.transform.position, transform.rotation);
            bullet.damage = damage;
            fireDelay = fireRate;
            ammo -= 1;
        }
    }
}
