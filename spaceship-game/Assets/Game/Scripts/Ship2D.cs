﻿using UnityEngine;
using System.Collections;

public class Ship2D : MonoBehaviour
{
    //Cache
    public Projectile2D bulletPrefab;

    public Rigidbody2D myRigidbody;
    protected Collider2D myCollider;

    public GameObject bulletSpawnPoint;

    public GameObject shipModel;

    //Info
    public int armor = 100;

    public float maxSpeed = 100;
    public float acceleration = 5;
    public float deceleration = 5;
    public float rotationAmount = 1;
    public float rotationSpeed = 10;

    public int normalAmmo = 1000000000;

    public float normalFireRate = 13;

    public int normalDamage = 10;

    //Internal
    public float normalFireDelay = 0;

    public Vector3 faceAngle;
    public float lookAngle;
    public Quaternion xQuaternion;
    public Quaternion zQuaternion;
    
    protected GameObject shipObj;

    public virtual void Awake()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        myCollider = GetComponent<Collider2D>();

        shipObj = new GameObject("Ship Model");
        shipObj.transform.position = transform.position;
        shipModel.transform.parent = shipObj.transform;
        shipModel.transform.localPosition = Vector3.zero;
    }

    public virtual void Update()
    {
        normalFireDelay -= Time.deltaTime;
    }

    public virtual void LateUpdate()
    {
        handleShipRotation();

        shipObj.transform.position = transform.position;
        shipObj.transform.rotation = zQuaternion*xQuaternion;
        
    }

    //Action
    public void fireNormal()
    {
        if(normalFireDelay <= 0 && normalAmmo > 0)
        {
            Projectile2D bullet = (Projectile2D)Instantiate(bulletPrefab, bulletSpawnPoint.transform.position, transform.rotation);
            bullet.damage = normalDamage;
            normalAmmo -= 1;
            normalFireDelay = normalFireRate;

            bullet.transform.rotation = Quaternion.Euler(0, 0, lookAngle);
        }
    }

    //Movement
    public void rotate(float rotationAmount)
    {
        lookAngle = Mathf.Lerp(lookAngle, lookAngle - rotationAmount, Time.deltaTime*rotationSpeed);
        transform.rotation = Quaternion.Euler(0, 0, lookAngle);
    }

    public void accelerate(float acceleration)
    {
        myRigidbody.AddForce(transform.right*Mathf.Abs(acceleration));

        if(myRigidbody.velocity.magnitude > maxSpeed)
            myRigidbody.velocity = myRigidbody.velocity.normalized*maxSpeed;
    }

    //Other
    public void damage(int damage)
    {
        armor -= damage;

        if(armor <= 0)
        {
            die();
        }
    }

    public virtual void die()
    {
        Destroy(shipObj);
        Destroy(gameObject);
        //Play particles and sound
    }

    public void handleShipRotation()
    {
        zQuaternion = Quaternion.Euler(0, 0, lookAngle);
        xQuaternion = Quaternion.Euler(correctAngle(lookAngle) <= 180 ? -correctAngle(lookAngle) : (correctAngle(lookAngle) - 180) - 180, 0, 0);
    }

    //Utils
    public float correctAngle(float angle)
    {
        if(angle < 0)
            return 360 + (angle % 360);
        else
            return angle % 360;
    }

    public void OnDrawGizmos()
    {
        if(Application.isPlaying)
            Gizmos.DrawRay(transform.position, myRigidbody.velocity);
    }
}
