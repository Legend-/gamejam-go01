﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BlackHole3D : MonoBehaviour
{
    public string levelName2D;

    public void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")
        {
            Application.LoadLevel(levelName2D);
        }
    }
}
