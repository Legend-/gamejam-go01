﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class WeakSpot3D : MonoBehaviour
{
    public int manualId;

    public float armor = 100;
    public static Dictionary<int, float> weakSpotList = new Dictionary<int, float>();

    public void Awake()
    {
        if(weakSpotList.ContainsKey(manualId))
        {
            armor = weakSpotList[manualId];

            if(armor <= 0)
                Destroy(gameObject);
        }
        else
            weakSpotList.Add(manualId, armor);
    }

    public void damage(int damage)
    {
        armor -= damage;

        weakSpotList[manualId] = armor;

        if(armor <= 0)
        {
            Base.baseList[manualId].hasShield = false;

            //Play stuff

            Destroy(gameObject);

            //Play stuff
        }
    }
}
