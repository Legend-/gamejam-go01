﻿using UnityEngine;
using System.Collections;

public class PlayerShip3D : Ship3D
{
    public static PlayerShip3D player;

    public Camera playerCamera;
    public GameObject missileSpawnPoint;
    public Projectile3D missilePrefab;

    //Info
    public float boost = 100;
    public float boostBurntPerSecond = 10;
    public float boostPerSercond = 20;
    public float hSpeed = 5;
    public float vSpeed = 5;
    public float interpolation = 4;
    public float missileFireRate = 10;
    public int missileAmmo = 5;
    public int missileDamage = 50;
    public float missileFireDelay = 0;

    private bool canBoost = true;

    //Internal
    private Missile3D missile;

    public override void Awake()
    {
        base.Awake();

        player = this;

        if(PlayerStats.armor != -10 && PlayerStats.normalAmmo != -10 && PlayerStats.missileAmmo != -10)
        {
            armor = PlayerStats.armor;
            normalAmmo = PlayerStats.normalAmmo;
            missileAmmo = PlayerStats.missileAmmo;
        }
    }

    public override void Update()
    {
        base.Update();

        maneuver(Input.GetAxis("Horizontal")*hSpeed, Input.GetAxis("Vertical")*vSpeed, interpolation);

        if(Input.GetKey(KeyCode.J))
        {
            fireNormal();
        }

        missileFireDelay -= Time.deltaTime;

        //Missile
        if(Input.GetKeyDown(KeyCode.K) && missile)
            fireMissile();
        else if(missileFireDelay <= 0 && missileAmmo > 0 && !missile)
        {
            missile = (Missile3D)Instantiate(missilePrefab, missileSpawnPoint.transform.position, transform.rotation);
            missile.transform.SetParent(missileSpawnPoint.transform, true);
            missile.myRigidbody.isKinematic = true;
            missile.enabled = false;
        }

        if(!canBoost)
        {
            boost += boostPerSercond*Time.deltaTime;

            if(boost >= 100)
                canBoost = true;
        }

        if(Input.GetKey(KeyCode.Space) && canBoost && boost > 0)
        {
            accelerate(acceleration*2);
            boost -= boostBurntPerSecond*Time.deltaTime;

            if(boost <= 0)
                canBoost = false;
        }
        else
            accelerate(acceleration);

        PlayerStats.armor = armor;
        PlayerStats.normalAmmo = normalAmmo;
        PlayerStats.missileAmmo = missileAmmo;
    }

    public override void die()
    {
        playerCamera.transform.SetParent(null, true);

        base.die();

        Menu.showGameover();
    }

    //Action
    public void fireMissile()
    {
        missile.damage = missileDamage;
        missile.myRigidbody.isKinematic = false;
        missileFireDelay = missileFireRate;
        missileAmmo -= 1;
        missile.enabled = true;

        //missile.transform.rotation = Quaternion.Euler(0, 0, lookAngle);
        missile.transform.SetParent(null, true);

        missile = null;
    }
}
