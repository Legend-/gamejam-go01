﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MotherShip : MonoBehaviour
{
    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            PlayerShip3D ship = collision.gameObject.GetComponent<PlayerShip3D>();
            ship.die();
        }
    }
}
