﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Missile3D : Projectile3D
{
    public ParticleSystem explosionPrefab;
    public float acceleration;
    public float maxSpeed;
    public float explosionRadius = 10;

    public void FixedUpdate()
    {
        myRigidbody.AddForce(transform.forward*Mathf.Abs(acceleration));

        if(myRigidbody.velocity.magnitude > maxSpeed)
            myRigidbody.velocity = myRigidbody.velocity.normalized*maxSpeed;
    }

    public override void postContact()
    {
        Instantiate(explosionPrefab, transform.position, transform.rotation);
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach(Collider collider in colliders)
        {
            if(collider.tag != targetTag)
                continue;

            Ship3D ship = collider.GetComponent<Ship3D>();

            if(ship)
            {
                ship.damage(damage);

                continue;
            }

            WeakSpot3D spot = collider.GetComponent<WeakSpot3D>();

            if(spot)
                spot.damage(damage);
        }
    }
}
