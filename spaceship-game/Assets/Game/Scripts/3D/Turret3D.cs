﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Turret3D : MonoBehaviour
{
    public Bullet3D bulletPrefab;
    public GameObject bulletSpawnPoint;

    public Transform target;
    public Transform gun;
    public float turretDegreesPerSecond = 45;
    public float gunDegreesPerSecond = 45;

    public float maxGunAngle = 45;

    public float attackRange = 45;
    public int ammo = 15;
    public int damage = 20;
    public float fireRate = 0.2f;
    private float fireDelay = 0;
    public float reloadTime = 5;
    public float reloadDelay = 0;
    public int reloadAmount = 15;
    public bool reloading = false;

    private Quaternion qTurret;
    private Quaternion qGun;
    private Quaternion qGunStart;
    private Transform trans;

    public void Start()
    {
        trans = transform;
        qGunStart = gun.transform.localRotation;
        target = PlayerShip3D.player.transform;
    }

    public void Update()
    {
        if(!PlayerShip3D.player)
            return;

        if(reloading)
        {
            reloadDelay -= Time.deltaTime;

            if(reloadDelay <= 0)
            {
                reloading = false;
                ammo = reloadAmount;
            }
        }
        else if(reloadDelay <= 0 && !reloading)
        {
            reloading = true;
            reloadDelay = reloadTime;
        }

        fireDelay -= Time.deltaTime;

        float distanceToPlane = Vector3.Dot(trans.up, target.position - trans.position);
        Vector3 planePoint = target.position - trans.up * distanceToPlane;

        qTurret = Quaternion.LookRotation(planePoint - trans.position, transform.up);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, qTurret, turretDegreesPerSecond * Time.deltaTime);

        Vector3 v3 = new Vector3(0, distanceToPlane, (planePoint - transform.position).magnitude);
        qGun = Quaternion.LookRotation(v3);

        if(Quaternion.Angle(qGunStart, qGun) <= maxGunAngle)
        {
            gun.localRotation = Quaternion.RotateTowards(gun.localRotation, qGun, gunDegreesPerSecond * Time.deltaTime);

            if(ammo > 0 && fireDelay <= 0 && Vector3.Distance(transform.position, target.position) < attackRange)
                fire();
        }
        else
        {
        }
    }

    public void fire()
    {
        Bullet3D bullet = (Bullet3D)Instantiate(bulletPrefab, bulletSpawnPoint.transform.position, gun.rotation);

        bullet.damage = damage;
        fireDelay = fireRate;
        ammo -= 1;
    }
}
