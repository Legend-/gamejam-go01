﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Projectile3D : MonoBehaviour
{
    //Cache
    public Rigidbody myRigidbody;
    public Collider myCollider;

    //Info
    public string targetTag;
    public int damage;
    public float lifeTime = 15;
    public bool damageOnContact = true;

    public void Update()
    {
        lifeTime -= Time.deltaTime;

        if(lifeTime <= 0)
            Destroy(gameObject);
    }

    public void Awake()
    {
        myRigidbody = GetComponent<Rigidbody>();
        myCollider = GetComponent<Collider>();
    }

    public void OnTriggerEnter(Collider collider)
    {
        if(!this.enabled)
            return;

        //Deal damage
        if(collider.tag == targetTag)
        {
            Ship3D ship = collider.GetComponent<Ship3D>();

            if(ship)
            {
                if(damageOnContact)
                    ship.damage(damage);

                postContact();
                destroy();

                return;
            }

            WeakSpot3D spot = collider.GetComponent<WeakSpot3D>();

            if(spot)
            {
                if(damageOnContact)
                    spot.damage(damage);
                
                postContact();
                destroy();
            }
        }
        else if(collider.tag != targetTag && collider.tag == "Untagged")
        {
            destroy();
            postContact();
        }
    }

    public void destroy()
    {
        //Play particles and sound

        Destroy(gameObject);
    }

    public virtual void postContact()
    {
    }
}
