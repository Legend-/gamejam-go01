﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Bullet3D : Projectile3D
{
    public float speed;

    public void FixedUpdate()
    {
        myRigidbody.MovePosition(transform.position + transform.forward*speed*Time.fixedDeltaTime);
    }
}
