﻿using UnityEngine;
using System.Collections;

public class Ship3D : MonoBehaviour
{
    //Cache
    public Projectile3D bulletPrefab;

    public Rigidbody myRigidbody;
    protected Collider myCollider;

    public GameObject bulletSpawnPoint;

    public GameObject modelShip;

    //Info
    public int armor = 100;

    //public float curSpeed = 20;
    //public float normalSpeed = 20;
    //public float doubleSpeed = 60;
    public float acceleration = 5;
    public float deceleration = 5;
    public float rotationAmount = 1;
    public float rotationSpeed = 10;

    public int normalAmmo = 1000000000;

    public float normalFireRate = 13;

    public int normalDamage = 10;

    //Internal
    public float normalFireDelay = 0;

    public float xRotation;
    public float yRotation;


    public virtual void Awake()
    {
        myRigidbody = GetComponent<Rigidbody>();
        myCollider = GetComponent<Collider>();
    }

    public virtual void Update()
    {
        normalFireDelay -= Time.deltaTime;
    }

    public virtual void LateUpdate()
    {
    }

    //Action
    public void fireNormal()
    {
        if(normalFireDelay <= 0 && normalAmmo > 0)
        {
            Projectile3D bullet = (Projectile3D)Instantiate(bulletPrefab, bulletSpawnPoint.transform.position, transform.rotation);
            bullet.damage = normalDamage;
            normalAmmo -= 1;
            normalFireDelay = normalFireRate;

            //bullet.transform.rotation = Quaternion.Euler(0, 0, lookAngle);
        }
    }

    //Movement
    public void maneuver(float hSpeed, float vSpeed, float interpolation)
    {
        Vector3 euler = transform.rotation.eulerAngles;
        xRotation = Mathf.Lerp(xRotation, xRotation + vSpeed, interpolation);
        yRotation = Mathf.Lerp(yRotation, yRotation + hSpeed, interpolation);
        transform.rotation = Quaternion.Euler(0, fixAngle(xRotation) >= 90 && fixAngle(xRotation) <= 270 ? -yRotation : yRotation, 0)*Quaternion.Euler(xRotation, 0, 0);

        modelShip.transform.localRotation = Quaternion.Euler(vSpeed*6, 0, -hSpeed*8);

        //transform.Rotate(new Vector3(vSpeed, hSpeed, 0));
        //transform.rotation = Quaternion.Euler(Mathf.Lerp(euler.y, euler.y + vSpeed, interpolation*Time.deltaTime), Mathf.Lerp(euler.x, euler.x + hSpeed, interpolation*Time.deltaTime), 0);
    }

    public void accelerate(float acceleration)
    {
        //myRigidbody.AddForce(transform.forward*Mathf.Abs(acceleration));
        myRigidbody.velocity = transform.forward*acceleration;

        //if(myRigidbody.velocity.magnitude > curSpeed)
        //    myRigidbody.velocity = myRigidbody.velocity.normalized*curSpeed;
    }

    //Other
    public void damage(int damage)
    {
        armor -= damage;

        if(armor <= 0)
        {
            die();
        }
    }

    public virtual void die()
    {
        Destroy(gameObject);
        //Play particles and sound
    }

    public float fixAngle(float angle)
    {
        if(angle >= 0)
            return angle % 360;
        else
            return 360 - (angle % 360);
    }

    //Utils
    public void OnDrawGizmos()
    {
        if(Application.isPlaying)
            Gizmos.DrawRay(transform.position, myRigidbody.velocity);
    }
}
