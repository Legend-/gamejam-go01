﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Pickup2D : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "Player")
        {
            pickup(collider.GetComponent<PlayerShip2D>());
            //Play sound

            Destroy(gameObject);
        }
    }

    public virtual void pickup(PlayerShip2D player)
    {
    }
}
