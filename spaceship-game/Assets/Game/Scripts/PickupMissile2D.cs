﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class PickupMissile2D : Pickup2D
{
    public override void pickup(PlayerShip2D player)
    {
        player.missileAmmo += 1;
    }
}
