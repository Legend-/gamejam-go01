﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum Sides
{
    Left,
    Right
}

public class EnemyShip2D : Ship2D
{
    public GameObject missileDrop;
    public GameObject shieldDrop;

    public Base owner;

    public float reloadTime = 5;
    private float reloadDelay = 0;
    public int reloadAmount = 6;
    public float dangerRange = 10;
    public float turnAroundRate = 5;
    private float turnAroundDelay;

    //Internal
    private PlayerShip2D playerTarget;
    private Vector3 targetRotation;
    public bool diveLock = false;
    public bool hiding = false;
    private bool reloading = false;

    public override void Awake()
    {
        base.Awake();

        turnAroundDelay = turnAroundRate;
    }

    public void Start()
    {
        playerTarget = PlayerShip2D.player;
    }

    public override void Update()
    {
        base.Update();

        AIFlight();

        if(reloading)
        {
            reloadDelay -= Time.deltaTime;

            if(reloadDelay <= 0)
            {
                normalAmmo = reloadAmount;
                reloading = false;
            }
        }
        else if(normalAmmo <= 0 && !reloading)
        {
            reloadDelay = reloadTime;
            reloading = true;
        }
    }

    public void OnDrawGizmos()
    {
        if(Application.isPlaying)
            Gizmos.DrawCube(targetRotation, Vector3.one);
    }

    public void AIFlight()
    {
        if(!playerTarget)
            return;

        RaycastHit2D ahead = Physics2D.Raycast(transform.position, transform.right, myRigidbody.velocity.magnitude);
        RaycastHit2D ground = Physics2D.Raycast(transform.position, Vector2.down, 5, LayerMask.GetMask("Ground"));

        float groundDistance = Vector2.Distance(ground.point, transform.position);

        if((ground.collider && groundDistance < 5) || (ahead.collider && ahead.collider.tag == "Ground"))
        {
            if(getSide() == Sides.Right)
                targetRotation = getTopRight();
            else
                targetRotation = getTopLeft();

            diveLock = false;
            hiding = false;

            rotateTowards(transform.position + targetRotation);
            accelerate(acceleration);
        }
        else
        {
            if(diveLock)
            {
                if(Vector2.Distance(targetRotation, transform.position) <= 5)
                {
                    diveLock = false;
                    hiding = true;

                    if(getSide() == Sides.Left)
                        targetRotation = transform.position + new Vector3(Random.Range(-10, -5), Random.Range(-10, -5), 0);
                    else
                        targetRotation = transform.position + new Vector3(Random.Range(5, 10), Random.Range(5, 10), 0);
                }

                rotateTowards(targetRotation);
                accelerate(acceleration*1.5f);

                if(Vector2.Angle(transform.right, playerTarget.transform.position - transform.position) < 5)
                    fireNormal();

                return;
            }

            if(hiding)
            {
                if(Vector2.Distance(targetRotation, transform.position) <= 35)
                {
                    hiding = false;
                }

                rotateTowards(targetRotation);
                accelerate(acceleration);

                return;
            }

            if(Vector2.Distance(playerTarget.transform.position, transform.position) < dangerRange*2 && Vector2.Angle(transform.right, playerTarget.transform.position - transform.position) < 15)
            {
                targetRotation = transform.position + (playerTarget.transform.position - transform.position)*Random.Range(3, 5);
                diveLock = true;
            }
            else
                targetRotation = playerTarget.transform.position;

            rotateTowards(targetRotation);
            accelerate(acceleration*1.5f);

            if(Vector2.Angle(transform.right, playerTarget.transform.position - transform.position) < 5)
                fireNormal();
        }
    }

    public override void die()
    {
        base.die();
        int number = Random.Range(1, 101);

        owner.curEnemies -= 1;

        if(number <= 25)
            Instantiate(missileDrop, transform.position, Quaternion.Euler(0, 0, 0));
        else if(number <= 70)
            Instantiate(shieldDrop, transform.position, Quaternion.Euler(0, 0, 0));
    }

    public Sides getSide()
    {
        return lookAngle > 270 || lookAngle < 90 ? Sides.Right : Sides.Left;
    }

    public void rotateTowards(Vector3 target)
    {
        Vector3 direction = target - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x)*Mathf.Rad2Deg;
        Quaternion rot = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * rotationAmount);
        lookAngle = transform.rotation.eulerAngles.z;
    }

    public Vector3 getRight()
    {
        return new Vector3(1, 0, 0);
    }

    public Vector3 getTop()
    {
        return new Vector3(0, 1, 0);
    }

    public Vector3 getLeft()
    {
        return new Vector3(-1, 0, 0);
    }

    public Vector3 getBottom()
    {
        return new Vector3(0, -1, 0);
    }

    public Vector3 getTopRight()
    {
        return new Vector3(1, 1, 0);
    }

    public Vector3 getTopLeft()
    {
        return new Vector3(-1, 1, 0);
    }

    public Vector3 getBottomLeft()
    {
        return new Vector3(-1, -1, 0);
    }

    public Vector3 getBottomRight()
    {
        return transform.position + new Vector3(1, -1, 0);
    }
}
