﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public static Menu menu;

    public GameObject hud;
    public Text armorLabel;
    public Text armorDisplay;
    public Text missileLabel;
    public Text missileDisplay;
    public Text boostLabel;
    public Text boostDisplay;

    public GameObject gameOver;
    public GameObject restart;

    public void Awake()
    {
        menu = this;
        //DontDestroyOnLoad(gameObject);
    }

    public void OnLevelWasLoaded(int level)
    {
        menu.hud.SetActive(true);
        menu.gameOver.SetActive(false);
        menu.restart.SetActive(false);

        if(Application.loadedLevelName == "Scene3D")
        {
            menu.boostLabel.gameObject.SetActive(true);
            menu.boostDisplay.gameObject.SetActive(true);
        }
        else
        {
            menu.boostLabel.gameObject.SetActive(false);
            menu.boostDisplay.gameObject.SetActive(false);
        }
    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        if(Input.GetKeyDown(KeyCode.F1))
        {
            Base.baseList.Clear();
            WeakSpot3D.weakSpotList.Clear();

            Base.score = 0;

            PlayerStats.armor = -10;
            PlayerStats.missileAmmo = -10;
            PlayerStats.normalAmmo = -10;

            Destroy(menu.gameObject);

            Application.LoadLevel("Scene2D");
        }

        if(PlayerShip2D.player)
        {
            armorDisplay.text = PlayerShip2D.player.armor.ToString();
            missileDisplay.text = PlayerShip2D.player.missileAmmo.ToString();
        }
        else if(PlayerShip3D.player)
        {
            armorDisplay.text = PlayerShip3D.player.armor.ToString();
            missileDisplay.text = PlayerShip3D.player.missileAmmo.ToString();
            boostDisplay.text = ((int)PlayerShip3D.player.boost).ToString();
        }
    }

    public static void showGameover()
    {
        menu.hud.SetActive(false);
        menu.gameOver.SetActive(true);
        menu.restart.SetActive(true);
    }
}
