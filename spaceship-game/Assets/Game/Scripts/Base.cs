﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BaseState
{
    public float armor;
    public bool hasShield;

    public BaseState(float armor, bool hasShield)
    {
        this.armor = armor;
        this.hasShield = hasShield;
    }
}

public class Base : MonoBehaviour
{
    public EnemyShip2D enemyPrefab;

    public List<GameObject> spawnPoints;
    public List<Turret2D> turrets;
    public Shield2D shield;

    public int manualId;
    public float spawnRate = 5;
    public int curEnemies = 0;
    public int maxEnemies = 10;
    private float spawnDelay;
    public float armor = 100;

    public static Dictionary<int, BaseState> baseList = new Dictionary<int, BaseState>();
    public static int score = 0;

    public void Awake()
    {
        if(baseList.ContainsKey(manualId))
        {
            armor = baseList[manualId].armor;

            if(armor <= 0)
            {
                foreach(Turret2D turret in turrets)
                    Destroy(turret.gameObject);

                Destroy(gameObject);
            }

            if(!baseList[manualId].hasShield)
                Destroy(shield.gameObject);
        }
        else
            baseList.Add(manualId, new BaseState(armor, true));
    }

    public void Update()
    {
        spawnDelay -= Time.deltaTime;

        if(spawnDelay <= 0 && curEnemies < maxEnemies)
        {
            GameObject spawn = spawnPoints[Random.Range(0, spawnPoints.Count)];
            spawnDelay = Random.Range(spawnRate/2, spawnRate);

            EnemyShip2D ship = (EnemyShip2D)Instantiate(enemyPrefab, spawn.transform.position, Quaternion.identity);
            ship.lookAngle = Random.Range(-1, 1)*90 + 90;
            ship.owner = this;
            ship.maxSpeed = Random.Range(ship.maxSpeed/2, ship.maxSpeed*1.2f);
            ship.acceleration = Random.Range(ship.acceleration/2, ship.acceleration*1.2f);

            curEnemies += 1;
        }
    }

    public void damage(int damage)
    {
        if(shield)
            return;

        armor -= damage;

        baseList[manualId].armor = armor;

        if(armor <= 0)
        {
            foreach(Turret2D turret in turrets)
            {
                Destroy(turret.gameObject);

                //Play Stuff
            }

            score += 1;

            if(score == 5)
                Menu.showGameover();

            Destroy(gameObject);

            //Play Stuff
        }
    }
}
