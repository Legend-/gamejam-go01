﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class PickupShield2D : Pickup2D
{
    public Shield2D shieldPrefab;
    public int shieldAmount = 30;

    public override void pickup(PlayerShip2D player)
    {
        if(player.shield)
            player.shield.shield += shieldAmount;
        else
        {
            player.shield = (Shield2D)Instantiate(shieldPrefab, player.transform.position, player.transform.rotation);
            player.shield.shield = shieldAmount;
        }
    }
}
