﻿using UnityEngine;
using System.Collections;

public class PlayerShip2D : Ship2D
{
    public static PlayerShip2D player;

    public Camera2D playerCamera;
    public GameObject missileSpawnPoint;
    public Projectile2D missilePrefab;
    public Shield2D shield;

    //Info
    public float missileFireRate = 10;
    public int missileAmmo = 5;
    public int missileDamage = 50;
    public float missileFireDelay = 0;

    //Internal
    private Missile2D missile;

    public override void Awake()
    {
        base.Awake();

        missileSpawnPoint.transform.parent = shipObj.transform;
        player = this;

        if(PlayerStats.armor != -10 && PlayerStats.normalAmmo != -10 && PlayerStats.missileAmmo != -10)
        {
            armor = PlayerStats.armor;
            normalAmmo = PlayerStats.normalAmmo;
            missileAmmo = PlayerStats.missileAmmo;
        }
    }

    public override void Update()
    {
        base.Update();

        rotate(Input.GetAxis("Horizontal")*rotationAmount);

        if(Input.GetKey(KeyCode.J))
        {
            fireNormal();
        }

        missileFireDelay -= Time.deltaTime;

        //Missile
        if(Input.GetKeyDown(KeyCode.K) && missile)
            fireMissile();
        else if(missileFireDelay <= 0 && missileAmmo > 0 && !missile)
        {
            missile = (Missile2D)Instantiate(missilePrefab, missileSpawnPoint.transform.position, transform.rotation);
            missile.transform.SetParent(missileSpawnPoint.transform, true);
            missile.myRigidbody.isKinematic = true;
            missile.enabled = false;
        }

        playerCamera.transform.position = new Vector3(transform.position.x, transform.position.y, -45);

        if(shield)
            shield.transform.position = transform.position;

        PlayerStats.armor = armor;
        PlayerStats.normalAmmo = normalAmmo;
        PlayerStats.missileAmmo = missileAmmo;
    }

    public void FixedUpdate()
    {
        accelerate(Input.GetAxis("Vertical")*acceleration);
    }

    public override void die()
    {
        base.die();

        Menu.showGameover();
    }

    //Action
    public void fireMissile()
    {
        missile.damage = missileDamage;
        missile.myRigidbody.isKinematic = false;
        missileFireDelay = missileFireRate;
        missileAmmo -= 1;
        missile.enabled = true;

        missile.transform.rotation = Quaternion.Euler(0, 0, lookAngle);
        missile.transform.SetParent(null, true);
        missile.myRigidbody.velocity = myRigidbody.velocity*0.9f;

        missile = null;
    }
}
