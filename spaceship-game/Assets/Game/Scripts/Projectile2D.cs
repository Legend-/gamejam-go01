﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Projectile2D : MonoBehaviour
{
    //Cache
    public Rigidbody2D myRigidbody;
    public Collider2D myCollider;

    //Info
    public string targetTag;
    public int damage;
    public float lifeTime = 15;
    public bool damageOnContact = true;

    public void Update()
    {
        lifeTime -= Time.deltaTime;

        if(lifeTime <= 0)
            Destroy(gameObject);
    }

    public void Awake()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        myCollider = GetComponent<Collider2D>();
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if(!this.enabled)
            return;

        //Deal damage
        if(collider.tag == targetTag)
        {
            Ship2D ship = collider.GetComponent<Ship2D>();

            if(ship)
            {
                if(damageOnContact)
                    ship.damage(damage);

                postContact();
                destroy();

                return;
            }

            Base enemyBase = collider.GetComponent<Base>();

            if(enemyBase)
            {
                if(damageOnContact)
                    enemyBase.damage(damage);

                postContact();
                destroy();

                return;
            }

            Shield2D shield = collider.GetComponent<Shield2D>();

            if(shield)
            {
                if(damageOnContact)
                    shield.shield -= damage;

                postContact();
                destroy();
            }
        }
        else if(collider.tag != targetTag && collider.tag != "Untagged")
            return;
    }

    public void destroy()
    {
        //Play particles and sound

        Destroy(gameObject);
    }

    public virtual void postContact()
    {
    }
}
