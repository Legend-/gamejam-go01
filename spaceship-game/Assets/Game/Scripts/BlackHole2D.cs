﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BlackHole2D : MonoBehaviour
{
    public string levelName3D;

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "Player")
        {
            Application.LoadLevel(levelName3D);
        }
    }
}
