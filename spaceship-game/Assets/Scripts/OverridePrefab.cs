﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class OverridePrefab : EditorWindow {

	[MenuItem("Tools/OverridePrefab")]
	public static void OpenWindow()
	{
		EditorWindow.GetWindow<OverridePrefab>().Show();
	}

	private bool byString = false;
	private string objectName;
	private GameObject prefabA;
	private GameObject prefabB;
	private List<GameObject> results = new List<GameObject>();

	void OnGUI()
	{
		results.Clear();
		byString = EditorGUILayout.Toggle("By String", byString);
		if( byString )
			objectName = EditorGUILayout.TextField(objectName);
		else
			prefabA = (GameObject)EditorGUILayout.ObjectField(prefabA, typeof(GameObject), false);
		prefabB = (GameObject)EditorGUILayout.ObjectField(prefabB, typeof(GameObject), false);
		GUI.enabled = ( (byString && !string.IsNullOrEmpty(objectName)) || (!byString  && prefabA != null) ) && prefabB != null;
		if( GUILayout.Button("Apply") )
		{
			GameObject[] obs = FindObjectsOfType<GameObject>();
			for( int i = obs.Length - 1 ; i >= 0 ; i-- )
			{
				GameObject g = obs[i];
				if( g == null ) continue;
				
				if( byString )
				{
					if( g.name.Contains(objectName) )
					{
						CreatePrefab(g);
					}
				}
				else
				{
					if( PrefabUtility.GetPrefabType(g) == PrefabType.PrefabInstance && PrefabUtility.GetPrefabParent(g)==prefabA )
					{
						/*var go = PrefabUtility.InstantiatePrefab( prefabB ) as GameObject;
						go.transform.position = g.transform.position;
						go.transform.rotation = g.transform.rotation;
						go.transform.SetParent(g.transform.parent);
						results.Add(go);
						//GameObject.Instantiate( prefabB, g.transform.position, g.transform.rotation );
						Undo.RegisterCreatedObjectUndo (go, "Created go");
						Undo.DestroyObjectImmediate(g);*/
						CreatePrefab(g);
						///PrefabUtility.InstantiatePrefab(
					}
				}
			}

			Selection.objects = results.ToArray();
			//PrefabUtility.GetPrefabType()
			//PrefabUtility.GetPrefabObject()
		}
	}

	private void CreatePrefab( GameObject g )
	{
		var go = PrefabUtility.InstantiatePrefab( prefabB ) as GameObject;
		go.transform.position = g.transform.position;
		go.transform.rotation = g.transform.rotation;
		go.transform.SetParent(g.transform.parent);
		results.Add(go);
		//GameObject.Instantiate( prefabB, g.transform.position, g.transform.rotation );
		Undo.RegisterCreatedObjectUndo (go, "Created go");
		Undo.DestroyObjectImmediate(g);
	}

}
